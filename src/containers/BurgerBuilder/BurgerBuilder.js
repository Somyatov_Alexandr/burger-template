import React, {Component} from 'react';
import Wrapper from "../../hoc/Wrapper";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";

const INGREDIENT_PRICES = {
  salad: 5,
  cheese: 20,
  meat: 50,
  bacon: 30
};

class BurgerBuilder extends Component {
  state = {
    ingredients: {
      bacon: 0,
      salad: 0,
      cheese: 0,
      meat: 0
    },
    totalPrice: 20,
    purchasable: false,
    purchasing: false
  };

  purchaseHandler = () => {
    this.setState({purchasing: true});
  };

  removePurchaseHandler = () => {
    this.setState({purchasing: false});
  };

  purchaseContinueHandler = () => {
    //[bacon=2]
    const queryParams = [];

    for (let key in this.state.ingredients) {
      if (this.state.ingredients.hasOwnProperty(key)) {
        queryParams.push(
            encodeURIComponent(key) + '=' + encodeURIComponent(this.state.ingredients[key])
        )
      }

    }

    queryParams.push('price' + '=' + this.state.totalPrice);

    const queryString = queryParams.join('&'); // bacon=2&chees=1&meat=5

    this.props.history.push({
      pathname: 'checkout',
      search: '?' + queryString
    }, this.state.ingredients)

  };

  updatePurchaseState = ingredients => {
    const sum = Object.keys(ingredients)
        .map(igKey => ingredients[igKey]).reduce((sum, el) => {
          return sum + el;
        }, 0);

    this.setState({purchasable: sum > 0});
  };

  addIngredientsHandler = type => {
    const oldCount = this.state.ingredients[type];
    const updateCount = oldCount + 1;
    const updateIngredients = {...this.state.ingredients};
    updateIngredients[type] = updateCount;

    const priceAddition = INGREDIENT_PRICES[type];
    const oldPrice = this.state.totalPrice;
    const newPrice = oldPrice + priceAddition;

    this.setState({totalPrice: newPrice, ingredients: updateIngredients});
    this.updatePurchaseState(updateIngredients);
  };

  removeIngredientsHandler = type => {
    const oldCount = this.state.ingredients[type];
    if (oldCount <= 0) return;
    const updateCount = oldCount - 1;
    const updateIngredients = {...this.state.ingredients};
    updateIngredients[type] = updateCount;

    const priceAddition = INGREDIENT_PRICES[type];
    const oldPrice = this.state.totalPrice;
    const newPrice = oldPrice - priceAddition;

    this.setState({totalPrice: newPrice, ingredients: updateIngredients});
    this.updatePurchaseState(updateIngredients);
  };

  render() {

    const disabledInfo = {...this.state.ingredients};
    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }

    return (
      <Wrapper>
        <Modal
            show={this.state.purchasing}
            closed={this.removePurchaseHandler}
        >
          <OrderSummary
              ingredients={this.state.ingredients}
              price={this.state.totalPrice}
              purchaseCancelled={this.removePurchaseHandler}
              purchaseContinued={this.purchaseContinueHandler}
          />
        </Modal>
        <Burger ingredients={this.state.ingredients}/>
        <BuildControls
            price={this.state.totalPrice}
            ingredientAdded={this.addIngredientsHandler}
            ingredientRemoved={this.removeIngredientsHandler}
            disabled={disabledInfo}
            purchasable={this.state.purchasable}
            ordered={this.purchaseHandler}
        />
      </Wrapper>
    )
  }
}

export default BurgerBuilder;