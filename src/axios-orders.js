import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-test25.firebaseio.com/'
});

export default instance;