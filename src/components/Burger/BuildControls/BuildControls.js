import React from 'react';
import './BuildControls.css';
import BuildControl from "./BuildControl/BuildControl";

const types = ['bacon', 'meat', 'salad', 'cheese'];

const BuildControls = props => {
  /** @namespace props.price */
  /** @namespace props.ordered */
  return (
      <div className="BuildControls">
        <p>Current Price: <strong>{props.price}</strong></p>
        {types.map(type => {
          return <BuildControl
              key={type}
              type={type}
              added={() => props.ingredientAdded(type)}
              removed={() => props.ingredientRemoved(type)}
              disabled={props.disabled[type]}
          />
        })}
        <button
            onClick={props.ordered}
            disabled={!props.purchasable}
            className="OrderButton"
        >Order</button>
      </div>
  );
};

export default BuildControls;