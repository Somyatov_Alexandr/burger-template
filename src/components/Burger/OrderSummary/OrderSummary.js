import React from 'react';
import Button from "../../UI/Button/Button";

const OrderSummary = props => {
  const ingredientSummary = Object.keys(props.ingredients)
      .map(igKey => {
        return <li key={igKey}>
          <span style={{textTransform: 'capitalize', marginRight: '5px'}}>
            {igKey}:
          </span>
            {props.ingredients[igKey]}
        </li>
      });
  return (
      <div>
        <h3>Ваш заказ</h3>
        <p>Выбранные вами ингредиенты:</p>
        <ul>
          {ingredientSummary}
        </ul>
        <p><strong>Total Price: {props.price} KGS</strong></p>
        <p>Продолжить оформление заказа?</p>
        <Button btnType="Danger" clicked={props.purchaseCancelled}>Отмена</Button>
        <Button btnType="Success" clicked={props.purchaseContinued}>Продолжить</Button>
      </div>
  );
};

export default OrderSummary;
